package com.parikhtech.atlassian.confluence.addon.servlet.filter;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.*;
import com.atlassian.plugin.servlet.PluginHttpRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AttachmentServletFilter implements Filter{
    private static final Logger log = LoggerFactory.getLogger(AttachmentServletFilter.class);
    private AttachmentManager attachmentManager;
    private PageManager pageManager;

    // When emailed, attachments have the following pattern:
    // http://<host>:<port>/<context_path>/download/attachments/<pageId>/<filename>
    // The filter is configured to intercept URLs matching "*/download/attachments/*" and will execute before login.
    private Pattern attachmentPattern = Pattern.compile(".*?/download/attachments/([0-9]*)/(.*)");

    public AttachmentServletFilter(AttachmentManager attachmentManager, PageManager pageManager) {
        this.attachmentManager = attachmentManager;
        this.pageManager = pageManager;
    }

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void destroy(){

    }

    public void doFilter(ServletRequest request,ServletResponse response,FilterChain chain)throws IOException,ServletException{
        final String path = ((PluginHttpRequestWrapper)request).getRequestURL().toString();

        // Wrap everything in a try/catch block. We don't want to impair functionality in the event there's an error.
        try {
            final Matcher m = attachmentPattern.matcher(path);

            // Do a quick check to ensure that the URL conforms to the expected pattern before proceeding.
            if (m.matches()) {
                final long pageId = Long.parseLong(m.group(1));
                final String attachmentFilename = m.group(2);

                final ContentEntityObject page = pageManager.getById(pageId);

                // Restrict attachment liberation to only blog posts for now...
                if (page != null && page instanceof BlogPost) {
                    // In order to retrieve the attachment, we need to attachment ID (???) OR the page the file is
                    // attached to and the filename.
                    final Attachment attachment = attachmentManager.getAttachment(page, attachmentFilename);

                    if (attachment != null) {
                        // Tell the browser what type of file this is
                        response.setContentType(attachment.getContentType());

                        // And let it know that it should be downloaded as an attachment. (Optional)
                        // Uncomment the line below to force the browser to download the file.
                        // ((HttpServletResponseWrapper)response).addHeader("Content-Disposition", "attachment; filename=\"" + attachmentFilename + "\"");

                        // Retrieve the attachment and stream it directly to the user, skipping the rest of the
                        // filter chain.
                        final InputStream attachmentStream = attachment.getContentsAsStream();
                        final OutputStream outputStream = response.getOutputStream();

                        int length = 0;
                        byte[] buffer = new byte[4096];
                        while (attachmentStream != null && (length = attachmentStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, length);
                        }
                        outputStream.close();
                        attachmentStream.close();
                        return;
                    }
                }
            }
        } catch (final ClassCastException e) {
            // Ignore these errors for now. They're mostly just noise.
        } catch (final Exception e) {
            log.error("Caught an exception while trying to stream attachment: " + path, e);
        }

        // Continue the request if anonymous attachment streaming failed for any reason
        chain.doFilter(request, response);
    }

}